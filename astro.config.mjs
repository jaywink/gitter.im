import { defineConfig } from 'astro/config';

import mdx from '@astrojs/mdx';

import rehypeAutolinkHeadings from 'rehype-autolink-headings';
import rehypeSlug from 'rehype-slug';
import { autolinkConfig } from './plugins/rehype-autolink-config';

// https://astro.build/config
export default defineConfig({
  integrations: [mdx()],
  markdown: {
    rehypePlugins: [
      // All of this is via:
      //  - https://jan-mueller.at/blog/next-level-heading-anchors/ and
      //  - https://github.com/withastro/docs/blob/cf8d464462be2021d293173bc562d8e79761148a/plugins/rehype-autolink-config.ts
      rehypeSlug,
      // This adds links to headings
      [rehypeAutolinkHeadings, autolinkConfig],
    ],
  },
});
